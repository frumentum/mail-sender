from typing import List

import pandas as pd
from pandas import DataFrame


def csv_processing(file_path: str) -> DataFrame:
    return pd.read_csv(file_path,
                       sep=";",
                       encoding="utf-8",
                       skiprows=None,
                       skip_blank_lines=True,
                       skipinitialspace=True,
                       dtype=str)


def get_index_of_email_column(receivers: DataFrame) -> int:
    col_names = receivers.columns
    if "email" in col_names:
        return col_names.get_loc("email")
    elif "Email" in col_names:
        return col_names.get_loc("Email")
    elif "email_address" in col_names.get_loc("email_address"):
        return col_names.get_loc("email_address")
    elif "Emailadresse" in col_names.get_loc("Emailadresse"):
        return col_names.get_loc("Emailadresse")
    else:
        raise ValueError("There is no column containing an email address. Valid column names are: email, Email, "
                         "email_address and Emailadresse.")
