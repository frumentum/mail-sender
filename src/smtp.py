import os.path
import smtplib
from email.mime.application import MIMEApplication
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from time import sleep
from typing import List

from pandas import DataFrame

from csv_processing import get_index_of_email_column
from txt_processing import replace_variables
from utils import Progress


class SMTPConfig:

    def __init__(self,
                 host: str,
                 port: int,
                 username: str,
                 password: str,
                 from_email: str,
                 mails_per_second: float,
                 ):
        self.host = host
        self.port = port
        self.username = username
        self.password = password
        self.from_email = from_email
        self.mails_per_second = mails_per_second


class SMTPService:

    def __init__(self, config: SMTPConfig):
        self.from_email = config.from_email
        self.timout = 1 / config.mails_per_second
        self.smtp = smtplib.SMTP(host=config.host, port=config.port)
        self.smtp.starttls()
        self.smtp.login(config.username, config.password)

    def close(self):
        self.smtp.close()

    def send_mail(self, subject: str, receiver: str, message: str, attachment: str):
        msg = MIMEMultipart()
        msg['Subject'] = subject
        msg['From'] = self.from_email
        msg['To'] = receiver
        msg.attach(MIMEText(message, _charset='utf-8'))

        if attachment is not None:
            with open(attachment, 'rb') as file:
                basename = os.path.basename(attachment)
                part = MIMEApplication(file.read(), Name=basename)
            part['Content-Disposition'] = 'attachment; filename="%s"' % basename
            msg.attach(part)

        self.smtp.sendmail(self.from_email, receiver, msg.as_string())

    def send_mails(self, subject: str, receivers: DataFrame, message: str, attachments: List[str]):
        progress = Progress(len(receivers.index), "Sending mails")
        for idx, row in receivers.iterrows():
            progress.update(idx)
            email = replace_variables(message, receivers, idx)
            index_of_email_col = get_index_of_email_column(receivers)
            if attachments is None:
                self.send_mail(
                    subject=subject,
                    receiver=row[index_of_email_col],
                    message=email,
                    attachment=None
                )
            elif len(receivers.index) == len(attachments):
                self.send_mail(
                    subject=subject,
                    receiver=row[index_of_email_col],
                    message=email,
                    attachment=attachments[idx]
                )
            else:
                ValueError("Attachments exist but not in same length than number of receivers.")
            sleep(self.timout)
