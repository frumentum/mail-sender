import getopt
import sys
from os import makedirs
from os.path import join
from shutil import rmtree

from csv_processing import csv_processing
from smtp import SMTPConfig, SMTPService
from txt_processing import txt_load
from utils import read_yaml, absolute_path


def cli(pwd, args):
    arg1 = ("-c", "receivers")
    arg2 = ("-t", "text")
    usage = "pdf-mailer %s <%s> %s <%s>" % (*arg1, *arg2)
    try:
        opts, args = getopt.getopt(args, "hc:t:", [arg1[1], arg2[1]])
    except getopt.GetoptError:
        print(usage)
        sys.exit(2)

    receivers = text = None

    for opt, arg in opts:
        print(opt, arg)
        if opt == '-h':
            print(usage)
            sys.exit()
        elif opt in (arg1[0], "--%s" % arg1[1]):
            receivers = absolute_path(arg, pwd)
            print("Path to receivers: %s" % receivers)
        elif opt in (arg2[0], "--%s" % arg2[1]):
            text = absolute_path(arg, pwd)
            print("Path to text: %s" % text)

    if receivers is None or text is None:
        print("one or all parameters was wrong")
        print(usage)
        sys.exit(2)

    return receivers, text


def execute(path_to_csv: str, path_to_txt: str, temp_dir: str, config_path: str):
    makedirs(temp_dir, exist_ok=True)
    receivers = csv_processing(path_to_csv)
    subject, email_template = txt_load(path_to_txt)

    smtp_config = SMTPConfig(**read_yaml(config_path)["smtp"])
    smtp_service = SMTPService(smtp_config)
    smtp_service.send_mails(
        receivers=receivers,
        subject=subject,
        message=email_template,
        attachments=None
    )
    smtp_service.close()
    rmtree(temp_dir)


def main():
    USER_DIR = sys.argv[1]
    TEMP_DIR = join(USER_DIR, "tmp")
    CONFIG = join(USER_DIR, "config.yaml")
    receivers, text = cli(sys.argv[2], sys.argv[3:])
    execute(
        path_to_csv=receivers,
        path_to_txt=text,
        temp_dir=TEMP_DIR,
        config_path=CONFIG
    )


if __name__ == "__main__":
    main()
