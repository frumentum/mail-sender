from pandas import DataFrame

from utils import german_form_of_address


def txt_load(file_path: str) -> (str, str):
    with open(file_path, ) as file:
        txt = file.read()
    return tuple(txt.split("\n", 1))

def txt_replace_variable(txt: str, key: str, value: str) -> str:
    return txt.replace("${%s}" % key, value)

def replace_variables(template: str, receivers: DataFrame, row_index: int) -> str:
    txt = template
    for col in receivers.columns:
        if col in ["Geschlecht", "sex"]:
            txt = txt_replace_variable(txt, col, german_form_of_address(receivers.at[row_index, col]))
        else:
            txt = txt_replace_variable(txt, col, receivers.at[row_index, col])
    if txt.find("${") != -1:
        raise ValueError("There are still some variables left in the mail template after variable replacement.")
    return txt

