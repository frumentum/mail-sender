from setuptools import setup, find_packages, find_namespace_packages

setup(
    name="mail-sender",
    version="0.1",
    setup_requires="setuptools",
    packages=['src'],
    include_package_data=True,
    entry_points={
        'console_scripts': [
            'mail-sender = src.main:main'
        ]
    }
)