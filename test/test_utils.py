from os.path import join

import pytest

from utils import Progress, absolute_path, german_form_of_address


class TestUtils:

    def test_progress(self):
        progress = Progress(400, "steps")
        for i in range(400):
            progress.update(i)
        print("sysout after progress")

    def test_absolute_path(self):
        pwd = "/some/path"
        result = absolute_path("somedir/somewhere", pwd)
        assert result == join(pwd, "somedir/somewhere")
        result = absolute_path("/absolute/path", pwd)
        assert result == "/absolute/path"

    def test_german_form_of_address(self):
        assert "Liebe" == german_form_of_address("f")
        assert "Liebe" == german_form_of_address("female")
        assert "Liebe" == german_form_of_address("w")
        assert "Liebe" == german_form_of_address("weiblich")
        assert "Lieber" == german_form_of_address("m")
        assert "Lieber" == german_form_of_address("male")
        assert "Lieber" == german_form_of_address("männlich")
        with pytest.raises(Exception):
            assert german_form_of_address("W")
        with pytest.raises(Exception):
            assert german_form_of_address("M")
        with pytest.raises(Exception):
            assert german_form_of_address("some_weird_value")