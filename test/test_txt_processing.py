from txt_processing import txt_load, txt_replace_variable, replace_variables

from csv_processing import csv_processing

def test_txt_load(email_txt):
    filepath = email_txt
    subject, txt = txt_load(filepath)
    assert subject == "Subject"
    assert txt == """${Geschlecht} ${Vorname} ${Nachname},

deine Rechnungsnummer ist die ${Rechnungsnummer} und der Gesamtpreis deiner Bestellung ist ${Gesamtpreis}

cheers"""


def test_txt_replace_variable():
    txt = "Hallo ${name}!"
    new = txt_replace_variable(txt, "name", "Simon")
    assert new == "Hallo Simon!"

def test_replace_variables(email_txt, email_receivers_csv):
    receivers = csv_processing(email_receivers_csv)
    subject, mail = txt_load(email_txt)
    alter_schwede = replace_variables(mail, receivers, 0)
    assert alter_schwede == """Lieber Alter Schwede,

deine Rechnungsnummer ist die RE-1234 und der Gesamtpreis deiner Bestellung ist 23.56

cheers"""

    opa_rudolf = replace_variables(mail, receivers, 1)
    assert opa_rudolf == """Lieber Opa Rudolf,

deine Rechnungsnummer ist die RE-5678 und der Gesamtpreis deiner Bestellung ist 59.75

cheers"""

    otto_walkes = replace_variables(mail, receivers, 3)
    assert otto_walkes == """Liebe Otto Walkes,

deine Rechnungsnummer ist die RE-5698 und der Gesamtpreis deiner Bestellung ist 456.98

cheers"""
