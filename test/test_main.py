from os.path import join

from pytest import mark

from main import main, cli, execute


class TestMain():

    @mark.skip
    def test_main(cli):
        main()

    def test_cli(self, email_receivers_csv, email_txt):
        args = ["-c", email_receivers_csv, "-t", email_txt]
        receivers, txt = cli("/", args)
        assert receivers == join("/", email_receivers_csv)
        assert txt == join("/", email_txt)

    def test_execute(self, email_receivers_csv, email_txt, config_yaml, tmp_dir):
        execute(
            path_to_csv=email_receivers_csv,
            path_to_txt=email_txt,
            temp_dir=tmp_dir,
            config_path=config_yaml
        )
