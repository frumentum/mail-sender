import pandas

from csv_processing import csv_processing, get_index_of_email_column


def test_csv_processing(email_receivers_csv):
    dict = {'Vorname': ["Alter", "Opa", "Axel", "Otto"], 'Nachname': ["Schwede", "Rudolf", "Schweiß", "Walkes"],
            'Geschlecht': ["m", "male", "männlich", "f"],
            'Email': ['alter_schwede@gmx.net', 'superopa@gmail.com', 'frumentum@posteo.de', 'ottowalkes@gmail.com'],
            'Gesamtpreis': ['23.56', '59.75', '123.78', '456.98'], 'Rechnungsnummer':
                ['RE-1234', 'RE-5678', 'RE-9877', 'RE-5698']}
    expected_df = pandas.DataFrame(data=dict)
    email_receivers = csv_processing(email_receivers_csv)
    assert expected_df.equals(email_receivers)


def test_get_index_of_email_column(email_receivers_csv):
    assert 3 == get_index_of_email_column(csv_processing(email_receivers_csv))
