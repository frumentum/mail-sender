import pandas


class TestSMTP:

    def test_send_mail(self, smtp_service):

        smtp_service.send_mail(
            subject="Subject",
            receiver="test@test.com",
            message="testmail",
            attachment=None
        )

    def test_send_mails(self, smtp_service, email_txt, email_receivers_csv):
        dict = {'first_name': ["Alter", "Opa", "Axel"], 'last_name': ["Schwede", "Rudolf", "Schweiß"],
                'email': ['alter_schwede@gmx.net', 'superopa@gmail.com', 'frumentum@posteo.de'],
                'total_amount': ['23.56', '59.75', '123.78'], 'invoice_number': ['RE-1234', 'RE-5678', 'RE-9877']}
        df = pandas.DataFrame(data=dict)
        smtp_service.send_mails(
            subject="TESTMAIL",
            receivers=df,
            message="testmail",
            attachments=[email_txt, email_receivers_csv, email_txt]
        )