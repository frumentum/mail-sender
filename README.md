# MAIL SENDER
mail sender takes a text file with placeholders and a csv containing information. The placeholders are replaced by the 
content of the csv row by row. That means that so many individual emails are sent than the number of rows of the csv.
## Installation
1. Download the installation script and run it
```shell script
wget -O - https://gitlab.com/frumentum/mail-sender/raw/master/scripts/install.sh?inline=false | bash
```

### setup smtp configurations
2. Enter your smtp configuration of your smtp server
```shell script
cd $HOME/.mail-sender
vim config.yaml
```

## Usage
To use the mail-sender you need to provide 2 files (filenames do not matter):
1. **email-receivers.csv:** the csv file must contain at least as many columns as different placeholders in the textfile.
That said a column name must match the name of a placeholder in the email template. The first row of the csv are the 
header (= column names). Since the german language differs depending on the gender of a person, you can specify a 
personal form of address for german mails (which means: "Lieber" or "Liebe" xy).
Gender can be given in four ways: "f", "w" or "m", "female" or "male", "weiblich" or "männlich".
The email columns has to be named as "Email", "email", "email_address" or "Emailadresse".
Example:
```csv
Vorname;Nachname;Geschlecht;Email;Rechnungsnummer;Gesamtpreis
peter;lustig;m;peter-lustig@gmx.net;RE-1234;12,34
list;funny;w;lisafunny@gmail.com;RE-5678;34,67
``` 
2. **email.txt:** 
the text of the email. The placeholders start with `${` and end with `}`. The content must match with a column name of 
the given csv. 
Example:
```text
${Geschlecht} ${Vorname} ${Nachname},
vielen Dank für deine Bestellung ${Rechnungsnummer} mit Gesamtpreis ${Gesamtpreis}
```

To start mail sender run following command from your terminal:
```shell script
mail-sender -c <path/to/receivers.csv> -t <path/to/email.txt>
```
## Development
it is recommended to use virtualenv with python3 on your system.

### Mailtrap
to test with real smtp server mailtrap works pretty well (https://mailtrap.io)
