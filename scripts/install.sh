#!/bin/bash
set -euo pipefail
IFS=$'\n\t'

echo "=> install system requirements"
sudo apt-get install -y git build-essential libssl-dev libffi-dev python3-dev python3-pip python3-venv

home_dir=$HOME/.mail-sender
opt_dir=/opt/mail-sender
symlink=/usr/local/bin/mail-sender

if [ -d "$opt_dir" ]; then
  echo "=> uninstall previous installation"
  sudo rm -rf $opt_dir
  rm -rf $home_dir
  sudo rm -rf $symlink
fi

echo "=> clone repo"
sudo git clone https://gitlab.com/frumentum/mail-sender.git /opt/mail-sender

echo "=> create .mail-sender"
mkdir -p $home_dir
cp $opt_dir/test/files/config.yaml $home_dir/

echo "=> setup virtualenv"
python3 -m venv $home_dir/venv_mail-sender

echo "=> activate virtualenv"
source $home_dir/venv_mail-sender/bin/activate

echo "=> install requirements"
pip install wheel
pip install -r $opt_dir/requirements.txt

echo "=> make symlink to /usr/local/bin"
sudo ln -s $opt_dir/scripts/mail-sender.sh $symlink