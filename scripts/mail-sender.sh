#!/bin/bash
set -euo pipefail
IFS=$'\n\t'

home_dir=$HOME/.mail-sender
opt_dir=/opt/mail-sender

echo "=> actviate venv"
source $home_dir/venv_mail-sender/bin/activate
PYTHONPATH=$opt_dir/src
echo "=> start mail-sender"
python $opt_dir/src/main.py $home_dir $(pwd) $@
